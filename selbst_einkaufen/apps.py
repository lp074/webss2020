from django.apps import AppConfig


class SelbstEinkaufenConfig(AppConfig):
    name = 'selbst_einkaufen'
